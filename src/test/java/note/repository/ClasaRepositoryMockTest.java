package note.repository;

import note.controller.NoteController;
import note.model.Elev;
import note.model.Medie;
import note.model.Nota;
import note.utils.ClasaException;
import note.utils.Constants;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class ClasaRepositoryMockTest {
    private NoteController ctrl;

    @Before
    public void init() {
        ctrl = new NoteController();
    }

    @Test
    public void testCase1() {
        try {
            ctrl.calculeazaMedii();
        } catch (ClasaException e) {
            Assert.assertEquals(Constants.emptyRepository, e.getMsg());
        }
    }

    @Test
    public void testCase4() throws ClasaException {

        Elev e = new Elev(1, "Andreica Eric");
        ctrl.addElev(e);
        Nota n = new Nota(1, "Matematica", 10);
        ctrl.addNota(n);
        ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
        List<Medie> rezultate = ctrl.calculeazaMedii();
        assertEquals(rezultate.size(),1);
        assertEquals(rezultate.get(0).getElev().getNume(),"Andreica Eric");
        assertEquals((int)rezultate.get(0).getMedie(),10);
    }

    @Test
    public void testCase2() throws ClasaException {

        Elev e = new Elev(1, "Andreica Eric");
        ctrl.addElev(e);
        ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
        List<Medie> rezultate = ctrl.calculeazaMedii();
        assertEquals(rezultate.size(),1);
        assertEquals(rezultate.get(0).getElev().getNume(),"Andreica Eric");
        assertTrue(Double.isNaN(rezultate.get(0).getMedie()));
    }

    @Test
    public void testCase3() throws ClasaException {

        Elev e = new Elev(1, "Andreica Eric");
        ctrl.addElev(e);
        Nota n = new Nota(2, "Matematica", 10);
        ctrl.addNota(n);
        ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
        List<Medie> rezultate = ctrl.calculeazaMedii();
        assertEquals(rezultate.size(),1);
        assertEquals(rezultate.get(0).getElev().getNume(),"Andreica Eric");
        assertTrue(Double.isNaN(rezultate.get(0).getMedie()));
    }

    @Test
    public void testCase5() throws ClasaException {

        Elev e1 = new Elev(1, "Andreica Eric");
        Elev e2 = new Elev(2, "Eric");
        Elev e3 = new Elev(3, "Andreica");

        ctrl.addElev(e1);
        ctrl.addElev(e2);
        ctrl.addElev(e3);

        Nota n1 = new Nota(1, "Matematica", 8);
        Nota n2 = new Nota(1, "Informatica", 8);
        Nota n3 = new Nota(1, "Fizica", 8);
        Nota n4 = new Nota(2, "Matematica", 10);
        Nota n5 = new Nota(2, "Informatica", 8);
        Nota n6 = new Nota(3, "Matematica", 1);

        ctrl.addNota(n1);
        ctrl.addNota(n2);
        ctrl.addNota(n3);
        ctrl.addNota(n4);
        ctrl.addNota(n5);
        ctrl.addNota(n6);

        ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
        List<Medie> rezultate = ctrl.calculeazaMedii();
        assertEquals(rezultate.size(),3);
        assertEquals(rezultate.get(0).getElev().getNume(),"Andreica Eric");
        assertEquals((int)rezultate.get(0).getMedie(),8);
        assertEquals(rezultate.get(1).getElev().getNume(),"Eric");
        assertEquals((int)rezultate.get(1).getMedie(),9);
        assertEquals(rezultate.get(2).getElev().getNume(),"Andreica");
        assertEquals((int)rezultate.get(2).getMedie(),1);

    }

}