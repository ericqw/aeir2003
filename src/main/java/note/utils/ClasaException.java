package note.utils;

public class ClasaException extends Exception {
    private String msg;

    public ClasaException(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }
}
